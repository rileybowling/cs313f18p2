TestList.java :

"Also try with a LinkedList - does it make any difference?"

It did not

"list.remove(5); //what does this do?"

This removes the element of the list at a given index, 5 in this case

"list.remove(Integer.valueOf(5)); //what about this one?"

Integer.valueOf(5) returns the integer value of the string '5' passed as an argument

TestIterator.java :

"what happens if you use list.remove(Integer.valueOf(77))?"

it does the same thing in effect, the three times 77 is found list.remove(Integer.valueOf(77)) removes the first
 element it finds with 77 in it 77 so elements at indexes 1, 3, and 5 are removed

 TestPerformance.java :

 Size - Runtime
 10     - 74ms,         Array Access:11ms, Array Add/Remove:30ms, Linked Access:14s 286ms, Linked Add/Remove:28ms,
 100    - 95ms,         Array Access:10ms, Array Add/Remove:38ms, Linked Access:24s 286ms, Linked Add/Remove:25ms,
 1000   - 536ms,        Array Access:12ms, Array Add/Remove:197ms, Linked Access:301ms, Linked Add/Remove:25ms,
 10000  - 5s981ms,      Array Access:13ms, Array Add/Remove:1s648ms, Linked Access:4s11ms, Linked Add/Remove:27ms,
 100000 - 1m19s375ms,   Array Access:33ms, Array Add/Remove:27s871ms, Linked Access:50s286ms, Linked Add/Remove:53ms,
